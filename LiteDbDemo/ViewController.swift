//
//  ViewController.swift
//  LiteDbDemo
//
//  Created by James Cash on 20-02-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let db = Database()

    override func viewDidLoad() {
        db.createTables()
        db.seedData()

        let info = db.fetch_all()
        print("Db info = \(info)")

        print("Reservations without clean water for 15 years: \(db.count_order_since(numberOfYears: 15))")
          print("Reservations without clean water for 20 years: \(db.count_order_since(numberOfYears: 20))")

        print("Names like 'W' \(db.names_like(query: "W"))")
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

