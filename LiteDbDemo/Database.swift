//
//  Database.swift
//  LiteDbDemo
//
//  Created by James Cash on 20-02-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
//import SQLite3

class Database {

    var conn: OpaquePointer?

    //https://www.sqlite.org/c3ref/bind_blob.html

    init() {
        let dbFileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("data.sqlite")


        if !FileManager.default.fileExists(atPath: dbFileURL.path) {
            // here, you could copy your pre-seeded db over
        }

        print("Using db file \(dbFileURL)")

        let err = sqlite3_open(dbFileURL.path, &conn)
        if err != SQLITE_OK {
            print("Unable to open database")
            abort()
        }
        prepare_fetch_all()
        prepare_count_query()
        prepare_names_like_query()
    }

    deinit {
        let err = sqlite3_close(conn!)
        if err != SQLITE_OK {
            print("Error closing database")
            abort()
        }

    }

    func createTables() {
        let query = "CREATE TABLE IF NOT EXISTS reservations (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT NOT NULL," +
            "boil_order_year INTEGER" +
        " );"
        exec_query(query: query)
    }

    // in reality, we probably wouldn't want to do this as a query
    // because it will re-run each time, so you end up with tons of copies
    // what you can do is check the database to see if you already have inserted stuff
    // OR
    // build a sqlite database yourself with the data
    // then on launch, check if you db file in documents already exists
    // and if doesn't, just copy your pre-made db from your app bundle to documents 
    func seedData() {
        /*
        let query = """INSERT INTO reservations (name, boil_order_year) VALUES
          ('Neskataga', 1995),
        """
        */
        let query = "INSERT INTO reservations (name, boil_order_year) VALUES " +
            // Leaving out the id column, but because it's the primary key, sqlite knows to just auto-increment the key
            "('Neskantaga', 1995)," +
            "('Shoal Lake #40', 2000)," +
            "('Eabametoong', 2001)," +
            "('North Spirit Lake', 2001)," +
            "('Northwest Angle', 2002)," +
            "('Sandy Lake', 2002)," +
            "('Lac Seul (Whitefish Bay)', 2002)," +
            "('Lac Seul (Kejick Bay)', 2003)," +
            "('Muskrat Dam', 2003)," +
            "('Wawakapewin', 2004)," +
            "('Slate Falls', 2004)"
        exec_query(query: query)

    }

    func exec_query(query: String) {
        var err: UnsafeMutablePointer<Int8>?
        let ret = sqlite3_exec(conn, query, nil, nil, &err)
        if ret != SQLITE_OK {
            let errStr = String(cString: err!)
            print("Error creating tables: \(errStr)")
            abort()
        }

    }

    var fetch_all_query: OpaquePointer?
    func prepare_fetch_all() {
        let query = "select name, boil_order_year from reservations;"

        let ret = sqlite3_prepare_v2(conn, query, -1, &fetch_all_query, nil)
        if ret != SQLITE_OK {
            print("Error preparing fetch all statement")
            abort()
        }
    }

    func fetch_all() -> [String:Int32] {
        var info = [String:Int32]()
        sqlite3_reset(fetch_all_query)

        var step_status = sqlite3_step(fetch_all_query)
        while (step_status == SQLITE_ROW) {
            let name = String(cString: sqlite3_column_text(fetch_all_query, 0))
            let year: Int32 = sqlite3_column_int(fetch_all_query, 1)

            info[name] = year

            step_status = sqlite3_step(fetch_all_query)
        }

        return info
    }

    var query_count: OpaquePointer?
    func prepare_count_query() {
        // the :num_years makes it a variable, so the same query can be run with different arguments
        // you can also use ? as a variable/placeholder
        let query = "select count(*) from reservations where (2018 - :num_years) > boil_order_year;"

        let ret = sqlite3_prepare_v2(conn, query, -1, &query_count, nil)
        if ret != SQLITE_OK {
            print("Error preparing fetch count statement")
            abort()
        }
    }

    func count_order_since(numberOfYears: Int32) -> Int32 {
        sqlite3_reset(query_count)
        sqlite3_clear_bindings(query_count)

        let numYearsIdx = sqlite3_bind_parameter_index(query_count, ":num_years")
        let err = sqlite3_bind_int(query_count, numYearsIdx, numberOfYears)
        if err != SQLITE_OK {
            print("Error binding params")
            abort()
        }

        let step_status = sqlite3_step(query_count)
        if step_status != SQLITE_ROW {
            print("Didn't get row from query")
            abort()
        }
        return sqlite3_column_int(query_count, 0)
    }

    var names_like_query: OpaquePointer?
    func prepare_names_like_query() {
        let query = "select * from reservations where name like '%' || :query || '%';"
        let ret = sqlite3_prepare_v2(conn, query, -1, &names_like_query, nil)
        if ret != SQLITE_OK {
            print("Error preparing fetch names like statement")
            abort()
        }
    }

    func names_like(query: String) -> [[String:Any]] {
        sqlite3_reset(names_like_query)
        sqlite3_clear_bindings(names_like_query)

        let queryIdx = sqlite3_bind_parameter_index(names_like_query, ":query")
        // SQLITE_TRANSIENT should already be defined I think, shouldn't need to do this...
        let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        let bindErr = sqlite3_bind_text(names_like_query, queryIdx, query, -1,
                          SQLITE_TRANSIENT)
        if bindErr != SQLITE_OK {
            print("Error binding parameter")
            abort()
        }

        var results = [[String:Any]]()
        var step_status = sqlite3_step(names_like_query)
        while (step_status == SQLITE_ROW) {
            var row = [String:Any]()
            for col in 0..<sqlite3_column_count(names_like_query) {
                let colName = String(cString: sqlite3_column_name(names_like_query, col))
                // exercise to the reader: use sqlite3_column_type which gives you a number reperesting the type, so you could make a switch statement on that value to figure out which column type to get
//                let type = sqlite3_column_type(names_like_query, col)
//                switch type {
//                    // look up docs
//                }
                let val: OpaquePointer = sqlite3_column_value(names_like_query, col)
                row[colName] = val
            }
            results.append(row)
            step_status = sqlite3_step(names_like_query)
        }

        return results
    }
}
